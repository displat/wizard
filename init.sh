#!/bin/bash

echo "Starting Displat deployment wizard!"
echo "Checking dependencies and preparing workspace...\n"

check_cmd() {
    command -v $1 >/dev/null 2>&1 || { echo >&2 "Dependency $1 is not found. Please install and try again."; exit 1; }
}

check_cmd git
check_cmd ssh
check_cmd curl
check_cmd unzip

mkdir -p _workspace/tools/

curl --proto '=https' -sSf --tlsv1.2 https://releases.hashicorp.com/terraform/0.14.4/terraform_0.14.4_linux_amd64.zip -o _workspace/tools/terraform_0.14.4.zip
unzip -d _workspace/tools/ _workspace/tools/terraform_0.14.4.zip

git clone 

echo "Alright, we're setup!\n"
while true; do
    read -p "Where are we deploying Displat?" host
    shopt -s nocasematch
    case $host in 
        do|digitalocean ) ./hosts/do.sh; break;;
        * ) echo "Please pick a valid choice - please refer to the documentation for supported hosts "
    esac
done
