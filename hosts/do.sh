#!/bin/bash

source _common.sh

echo "Ok, DigitalOcean! Give us a moment to setup...\n"

mkdir -p _workspace/terraform/digitalocean
git clone https://gitlab.com/displat/infra/terraform/digitalocean.git _workspace/terraform/digitalocean

if [[ -z "${DIGITALOCEAN_TOKEN}" ]] && [[ -z "${DIGITALOCEAN_ACCESS_TOKEN}" ]]; then
    read -s -p "Please provide your DigitalOcean token (input hidden):" DIGITALOCEAN_TOKEN
fi

echo "Performing Terraform plan for a single-node AIO setup. More setups will be supported over time."

tf_plan digitalocean digitalocean_droplet.mono_aio


